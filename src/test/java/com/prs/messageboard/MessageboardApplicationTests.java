package com.prs.messageboard;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootTest
@EnableTransactionManagement
class MessageboardApplicationTests {

	@Test
	void contextLoads() {
	}

}
