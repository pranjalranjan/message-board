package com.prs.messageboard.filters;

import com.prs.messageboard.service.user.JwtService;
import com.prs.messageboard.service.user.MessageBoardUserDetailsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Optional;

public class RequestAuthenticationFilterTest
{
    private MessageBoardUserDetailsService mockMessageBoardUserDetailsService = Mockito.mock(MessageBoardUserDetailsService.class);
    private JwtService jwtService = new JwtService();
    private RequestAuthenticationFilter requestAuthenticationFilter = new RequestAuthenticationFilter(mockMessageBoardUserDetailsService, jwtService);

    @Test
    public void testgetUsernameFromRequestByAuthorizationHeader_ValidTokenInHeader() {
        String jwt = ReflectionTestUtils.invokeMethod(jwtService, "createToken", new HashMap<>(), "SOME_TEST_SUBJECT");
        Optional<String> userName = ReflectionTestUtils.invokeMethod(requestAuthenticationFilter, "getUsernameFromRequestByAuthorizationHeader", "Bearer "+jwt);

        Assertions.assertTrue(userName.isPresent());
        Assertions.assertEquals("SOME_TEST_SUBJECT", userName.get());
    }

    @Test
    public void testgetUsernameFromRequestByAuthorizationHeader_InValidTokenInHeader() {
        String jwt = ReflectionTestUtils.invokeMethod(jwtService, "createToken", new HashMap<>(), "SOME_TEST_SUBJECT");
        Optional<String> userName1 = ReflectionTestUtils.invokeMethod(requestAuthenticationFilter, "getUsernameFromRequestByAuthorizationHeader", "Bearer"+jwt);
        Optional<String> userName2 = ReflectionTestUtils.invokeMethod(requestAuthenticationFilter, "getUsernameFromRequestByAuthorizationHeader", "bearer"+jwt);
        Optional<String> userName3 = ReflectionTestUtils.invokeMethod(requestAuthenticationFilter, "getUsernameFromRequestByAuthorizationHeader", "Bearer:"+jwt);
        Optional<String> userName4 = ReflectionTestUtils.invokeMethod(requestAuthenticationFilter, "getUsernameFromRequestByAuthorizationHeader", "Bearer-"+jwt);
        Optional<String> userName5 = ReflectionTestUtils.invokeMethod(requestAuthenticationFilter, "getUsernameFromRequestByAuthorizationHeader", jwt);

        Assertions.assertFalse(userName1.isPresent());
        Assertions.assertFalse(userName2.isPresent());
        Assertions.assertFalse(userName3.isPresent());
        Assertions.assertFalse(userName4.isPresent());
        Assertions.assertFalse(userName5.isPresent());
    }
}
