package com.prs.messageboard.integrationtests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prs.messageboard.controllers.message.MessageController;
import com.prs.messageboard.models.message.Message;
import com.prs.messageboard.service.commons.TraceIdService;
import com.prs.messageboard.service.message.MessageService;
import com.prs.messageboard.service.user.JwtService;
import com.prs.messageboard.service.user.MessageBoardUserDetailsService;
import com.prs.messageboard.service.user.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

@WebMvcTest(MessageController.class)
@MockBean(MessageBoardUserDetailsService.class)
@MockBean(JwtService.class)
@MockBean(TraceIdService.class)
public class MessageControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MessageService messageService;

    @WithMockUser("TEST_USERNAME")
    @Test
    public void testgetAllMessages_shouldSuceed() throws Exception{
        Mockito.reset(messageService);
        Mockito.when(messageService.getMessagesForAllUsers()).thenReturn(new ArrayList<>());
        mockMvc.perform(MockMvcRequestBuilders.get("/message").contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @WithMockUser("TEST_USERNAME")
    @Test
    public void testdeleteMessage_shouldSucceed() throws Exception {
        Mockito.reset(messageService);

        Mockito.doNothing().when(messageService).validateMessageAccessAndDelete(Matchers.eq(1), Matchers.any());
        mockMvc.perform(MockMvcRequestBuilders.delete("/message/1").contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @WithMockUser("TEST_USERNAME")
    @Test
    public void testcreateMessage_shouldSucceed() throws Exception {
        Message message = new Message();
        message.setText("This is a new message");
        Mockito.reset(messageService);

        Mockito.when(messageService.createNewMessageByUsername(Matchers.any(), Matchers.any())).thenReturn(message);
        mockMvc.perform(MockMvcRequestBuilders.post("/message").content((new ObjectMapper()).writeValueAsString(message)).contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @WithMockUser("TEST_USERNAME")
    @Test
    public void testupdateMessage_shouldSucceed() throws Exception {
        Message newMessage = new Message();
        newMessage.setText("This is an updated message");
        Mockito.reset(messageService);

        Mockito.when(messageService.validateMessageAccessAndUpdateText(Matchers.any(), Matchers.eq(1), Matchers.any())).thenReturn(newMessage);
        mockMvc.perform(MockMvcRequestBuilders.put("/message/1").content((new ObjectMapper()).writeValueAsString(newMessage)).contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
