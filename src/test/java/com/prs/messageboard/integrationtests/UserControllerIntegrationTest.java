package com.prs.messageboard.integrationtests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prs.messageboard.controllers.user.UserController;
import com.prs.messageboard.models.user.User;
import com.prs.messageboard.service.commons.TraceIdService;
import com.prs.messageboard.service.user.JwtService;
import com.prs.messageboard.service.user.MessageBoardUserDetailsService;
import com.prs.messageboard.service.user.UserService;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;

@WebMvcTest(UserController.class)
@MockBean(MessageBoardUserDetailsService.class)
@MockBean(JwtService.class)
@MockBean(TraceIdService.class)
public class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @WithMockUser("TEST_USERNAME")
    @Test
    public void testgetAllUsers_shouldSuceed() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/user").contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @WithMockUser("TEST_USERNAME")
    @Test
    public void testgetUserById_shouldSuceed() throws Exception{
        User toBeFoundUser = new User();
        toBeFoundUser.setId(1);
        toBeFoundUser.setUsername("TEST_USERNAME");
        toBeFoundUser.setPassword("TEST_PASSWORD");
        toBeFoundUser.setBirthDate(new Date());

        Mockito.reset(userService);
        Mockito.when(userService.getUserById(1)).thenReturn(toBeFoundUser);
        mockMvc.perform(MockMvcRequestBuilders.get("/user/1").contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @WithMockUser("TEST_USERNAME")
    @Test
    public void testcreateUsers_shouldSuceed() throws Exception{
        User toBeCreatedUser = new User();
        toBeCreatedUser.setId(1);
        toBeCreatedUser.setUsername("TEST_USERNAME");
        toBeCreatedUser.setPassword("TEST_PASSWORD");
        toBeCreatedUser.setBirthDate(new Date());

        Mockito.reset(userService);
        Mockito.when(userService.createNewUser(Matchers.any())).thenReturn(toBeCreatedUser);
        mockMvc.perform(MockMvcRequestBuilders.post("/user").content((new ObjectMapper()).writeValueAsString(toBeCreatedUser)).contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @WithMockUser("TEST_USERNAME")
    @Test
    public void testdeleteUsers_shouldSuceed() throws Exception{
        Mockito.reset(userService);
        Mockito.doNothing().when(userService).deleteUserById(Matchers.any());
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/1").contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
    }


}
