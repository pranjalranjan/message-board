package com.prs.messageboard.service.user;

import com.prs.messageboard.exception.user.UserNotFoundException;
import com.prs.messageboard.models.user.User;
import com.prs.messageboard.repositories.message.MessageRepository;
import com.prs.messageboard.repositories.user.UserRepository;
import com.prs.messageboard.service.message.MessageService;
import com.prs.messageboard.service.user.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class UserServiceTest {
    UserRepository mockUserRepository = Mockito.mock(UserRepository.class);
    MessageService mockMessageService = Mockito.mock(MessageService.class);
    UserService userService = new UserService(mockUserRepository, mockMessageService);

    @BeforeEach
    void resetMockedConstructs() {
        Mockito.reset(mockUserRepository);
        Mockito.reset(mockMessageService);
    }

    @Test
    void testgetUserByUsername_ValidCase() {
        User testUser = new User();
        testUser.setUsername("TEST_USER");
        Mockito.when(mockUserRepository.findByUsername(Matchers.any())).thenReturn(testUser);

        Assertions.assertEquals(testUser, userService.getUserByUsername("TEST_USER"), "User service returns the exact user by username");
    }

    @Test
    void testgetUserByUsername_NonExistantUsername() {
        User testUser = new User();
        testUser.setUsername("TEST_USER");
        Mockito.when(mockUserRepository.findByUsername(Matchers.any())).thenReturn(null);

        Assertions.assertNull(userService.getUserByUsername("TEST_USER"), "user service returns null if user doesn't exist in data store");
    }

    @Test
    void testgetAllUsers_ValidCase() {
        User testUser1 = new User();
        testUser1.setUsername("TEST_USER");
        User testUser2 = new User();
        testUser2.setUsername("TEST_USER_2");
        Mockito.when(mockUserRepository.findAll()).thenReturn(Arrays.asList(testUser1, testUser2));

        Assertions.assertNotNull(userService.getAllUsers(), "user service returns non-null list if users are present in data store.");
        Assertions.assertEquals(2, userService.getAllUsers().size(), "user service returns non-empty list if users are present in data store.");
        Assertions.assertTrue(userService.getAllUsers().contains(testUser1), "user service returns list which contains the users present in data store.");
        Assertions.assertTrue(userService.getAllUsers().contains(testUser2), "user service returns list which contains the users present in data store.");
    }

    @Test
    void testgetAllUsers_EmptyUserDataStore() {
        Mockito.when(mockUserRepository.findAll()).thenReturn(new ArrayList<>());
        Assertions.assertNotNull(userService.getAllUsers(), "user service returns non-null list if users are not present in data store.");
        Assertions.assertEquals(0, userService.getAllUsers().size(), "user service returns empty list if users are not present in data store.");
        Assertions.assertTrue(userService.getAllUsers().isEmpty());
    }

    @Test
    void testgetUserById_ValidCase() {
        User testUser = new User();
        testUser.setUsername("TEST_USER");
        testUser.setId(1);
        Mockito.when(mockUserRepository.findById(1)).thenReturn(Optional.of(testUser));

        Assertions.assertEquals(testUser, userService.getUserById(1), "User service returns the exact user by id");
    }

    @Test
    void testgetUserById_NonExistantUser() {
        User testUser = new User();
        testUser.setUsername("TEST_USER");
        testUser.setId(1);
        Mockito.when(mockUserRepository.findById(1)).thenReturn(Optional.of(testUser));

        //First check that assertion passes with correct Id
        Assertions.assertEquals(testUser, userService.getUserById(1), "User service returns the exact user by id");

        //Assert failure when user with Id does not exist in data store,
        Assertions.assertThrows(UserNotFoundException.class, () -> userService.getUserById(2), "User service throws UserNotFoundException when user with id doesn't exist in data store");
    }

    @Test
    void testcreateNewUser_ValidCase() {
        User toBeCreatedUser = new User();
        User createdUser = new User();
        createdUser.setId(1);

        Mockito.when(mockUserRepository.save(Matchers.any())).thenReturn(createdUser);

        //
        Assertions.assertNull(toBeCreatedUser.getId());
        Assertions.assertEquals(1, userService.createNewUser(toBeCreatedUser).getId());
    }

    @Test
    void testDeleteUser_ValidCase() {
        User toBeDeletedUser = new User();
        toBeDeletedUser.setId(1);
        toBeDeletedUser.setMessages(new ArrayList<>());

        Mockito.when(mockUserRepository.findById(1)).thenReturn(Optional.of(toBeDeletedUser));
        Mockito.doNothing().when(mockUserRepository).deleteById(1);
        Mockito.doNothing().when(mockMessageService).validateMessageAccessAndDelete(Matchers.anyInt(), Matchers.any());

        Assertions.assertDoesNotThrow(() -> userService.deleteUserById(1));
    }
}
