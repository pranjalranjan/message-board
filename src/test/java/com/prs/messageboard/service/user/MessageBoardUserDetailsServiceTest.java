package com.prs.messageboard.service.user;

import com.prs.messageboard.exception.user.UserNotFoundException;
import com.prs.messageboard.models.user.User;
import com.prs.messageboard.repositories.user.UserRepository;
import com.prs.messageboard.service.user.MessageBoardUserDetailsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

public class MessageBoardUserDetailsServiceTest {
    UserRepository mockUserRepository = Mockito.mock(UserRepository.class);
    MessageBoardUserDetailsService messageBoardUserDetailsService = new MessageBoardUserDetailsService(mockUserRepository);

    @BeforeEach
    void resetMockedConstructs() {
        Mockito.reset(mockUserRepository);
    }

    @Test
    public void testloadUserByUsername_ValidCase() {
        User testUser = new User();
        testUser.setUsername("TEST_USERNAME");
        testUser.setPassword("TEST_PASSWORD");
        Mockito.when(mockUserRepository.findByUsername(Matchers.any())).thenReturn(testUser);

        org.springframework.security.core.userdetails.UserDetails returnedUserDetails = messageBoardUserDetailsService.loadUserByUsername("TEST_USERNAME");
        Assertions.assertEquals(returnedUserDetails.getUsername(), "TEST_USERNAME");
        Assertions.assertEquals(returnedUserDetails.getPassword(), "TEST_PASSWORD");
    }

    @Test
    public void testloadUserByUsername_NonExistantUser() {
        User testUser = new User();
        testUser.setUsername("TEST_USERNAME");
        testUser.setPassword("TEST_PASSWORD");
        Mockito.when(mockUserRepository.findByUsername(Matchers.any())).thenReturn(null);

        Assertions.assertThrows(UserNotFoundException.class, ()-> messageBoardUserDetailsService.loadUserByUsername("TEST_USERNAME"));
    }
}
