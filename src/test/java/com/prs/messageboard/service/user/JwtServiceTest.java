package com.prs.messageboard.service.user;

import com.prs.messageboard.service.user.JwtService;
import io.jsonwebtoken.MalformedJwtException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.util.AssertionErrors;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

public class JwtServiceTest
{
    private JwtService jwtService = new JwtService();

    private UserDetails userDetails = new UserDetails() {
        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return null;
        }

        @Override
        public String getPassword() {
            return null;
        }

        @Override
        public String getUsername() {
            return "TEST_USER";
        }

        @Override
        public boolean isAccountNonExpired() {
            return false;
        }

        @Override
        public boolean isAccountNonLocked() {
            return false;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return false;
        }

        @Override
        public boolean isEnabled() {
            return false;
        }
    };

    @Test
    public void testcreateToken_ValidCase() {
        String jwt = ReflectionTestUtils.invokeMethod(jwtService, "createToken", new HashMap<>(), "SOME_TEST_SUBJECT");

        Assertions.assertNotNull(jwt);
        Assertions.assertFalse(jwt.trim().isEmpty());
    }

    @Test
    public void testgenerateToken_ValidCase() {
        String jwt = jwtService.generateToken(userDetails);

        Assertions.assertNotNull(jwt);
        Assertions.assertFalse(jwt.trim().isEmpty());
    }

    @Test
    public void testextractUsername_ValidCase() {
        String jwt = ReflectionTestUtils.invokeMethod(jwtService, "createToken", new HashMap<>(), "SOME_TEST_SUBJECT");

        String extractedUsername = jwtService.extractUsername(jwt);
        Assertions.assertNotNull(extractedUsername);
        Assertions.assertEquals("SOME_TEST_SUBJECT", extractedUsername);
    }

    @Test
    public void testextractExpiration_ValidCase(){
        String jwt = ReflectionTestUtils.invokeMethod(jwtService, "createToken", new HashMap<>(), "SOME_TEST_SUBJECT");

        Date extractedExpiration = jwtService.extractExpiration(jwt);

        Assertions.assertTrue(extractedExpiration.after(new Date()));
    }

    @Test
    public void testvalidateToken_ValidCase() {
        String jwt = ReflectionTestUtils.invokeMethod(jwtService, "createToken", new HashMap<>(), "TEST_USER");

        Assertions.assertTrue(jwtService.validateToken(jwt, userDetails));
    }

    @Test
    public void testvalidateToken_RandomStringInvalidCase() {
        Assertions.assertThrows(MalformedJwtException.class, () -> jwtService.validateToken("SOME_RANDOM_STRING", userDetails));
        Assertions.assertThrows(MalformedJwtException.class, ()->jwtService.validateToken("SOME.JSON_TOKEN.REPLICA", userDetails));
    }

    @Test
    public void testvalidateToken_ValidTokenForDifferentUser() {
        String jwt = ReflectionTestUtils.invokeMethod(jwtService, "createToken", new HashMap<>(), "SOME_TEST_SUBJECT");

        Assertions.assertFalse(jwtService.validateToken(jwt, userDetails));
    }
}
