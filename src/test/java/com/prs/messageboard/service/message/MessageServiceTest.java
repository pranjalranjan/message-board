package com.prs.messageboard.service.message;

import com.prs.messageboard.exception.message.MessageNotFoundException;
import com.prs.messageboard.exception.message.UnauthorizedMessageAccessException;
import com.prs.messageboard.exception.user.UserNotFoundException;
import com.prs.messageboard.models.message.Message;
import com.prs.messageboard.models.user.User;
import com.prs.messageboard.repositories.message.MessageRepository;
import com.prs.messageboard.repositories.user.UserRepository;
import com.prs.messageboard.service.user.MessageBoardUserDetailsService;
import com.prs.messageboard.service.user.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MessageServiceTest
{
    MessageRepository mockMessageRepository = Mockito.mock(MessageRepository.class);
    UserService mockUserService = Mockito.mock(UserService.class);
    MessageService messageService = new MessageService(mockMessageRepository, mockUserService);

    @BeforeEach
    void resetMockedConstructs() {
        Mockito.reset(mockMessageRepository);
        Mockito.reset(mockUserService);
    }

    @Test
    public void testgetMessageByMessageIdAndUserName_NonExistantMessage() {
        Mockito.when(mockMessageRepository.findById(Matchers.any())).thenReturn(Optional.empty());

        Assertions.assertThrows(MessageNotFoundException.class, ()->messageService.getMessageByMessageIdAndUserName(1, "SOME_TEST_USERNAME"));
    }

    @Test
    public void testgetMessageByMessageIdAndUserName_UnauthorizedAccess() {
        Message message = new Message();
        User user = new User();
        user.setUsername("TEST_USER_1");
        message.setUser(user);
        Mockito.when(mockMessageRepository.findById(Matchers.any())).thenReturn(Optional.of(message));

        Assertions.assertThrows(UnauthorizedMessageAccessException.class, ()->messageService.getMessageByMessageIdAndUserName(1, "TEST_USER_2"));
    }

    @Test
    public void testgetMessageByMessageIdAndUserName_ValidCase() {
        User user = new User();
        user.setUsername("TEST_USER_1");

        Message message = new Message();
        message.setId(1);
        message.setUser(user);
        Mockito.when(mockMessageRepository.findById(Matchers.any())).thenReturn(Optional.of(message));

        Assertions.assertNotNull(messageService.getMessageByMessageIdAndUserName(1, "TEST_USER_1"));
        Assertions.assertEquals(message, messageService.getMessageByMessageIdAndUserName(1, "TEST_USER_1"));
    }

    @Test
    public void testvalidateMessageAccessAndUpdateText_UnauthorizedAccess() {
        User user = new User();
        user.setUsername("TEST_USER_1");
        Message message = new Message();
        message.setId(1);
        message.setText("Old text");
        message.setUser(user);

        Message newMessage = new Message();
        newMessage.setId(1);
        newMessage.setText("New text");
        newMessage.setUser(user);
        Mockito.when(mockMessageRepository.findById(Matchers.any())).thenReturn(Optional.of(message));

        Assertions.assertThrows(UnauthorizedMessageAccessException.class, ()->messageService.validateMessageAccessAndUpdateText(newMessage, 1, "TEST_USER_2"));
    }

    @Test
    public void testvalidateMessageAccessAndUpdateText_ValidCase() {
        User user = new User();
        user.setUsername("TEST_USER_1");
        Message message = new Message();
        message.setId(1);
        message.setText("Old text");
        message.setUser(user);

        Message newMessage = new Message();
        newMessage.setId(1);
        newMessage.setText("New text");
        newMessage.setUser(user);
        Mockito.when(mockMessageRepository.findById(Matchers.any())).thenReturn(Optional.of(message));
        Mockito.when(mockMessageRepository.save(Matchers.any())).thenReturn(newMessage);

        Assertions.assertNotNull(messageService.validateMessageAccessAndUpdateText(newMessage, 1, "TEST_USER_1"));
        Assertions.assertEquals("New text", messageService.validateMessageAccessAndUpdateText(newMessage, 1, "TEST_USER_1").getText());
    }

    @Test
    public void testvalidateMessageAccessAndDelete_UnauthorizedAccess() {
        User user = new User();
        user.setUsername("TEST_USER_1");
        Message message = new Message();
        message.setId(1);
        message.setText("Text");
        message.setUser(user);
        Mockito.when(mockMessageRepository.findById(Matchers.any())).thenReturn(Optional.of(message));

        Assertions.assertThrows(UnauthorizedMessageAccessException.class, ()->messageService.validateMessageAccessAndDelete(1, "TEST_USER_2"));
    }

    @Test
    public void testvalidateMessageAccessAndDelete_ValidCase() {
        User user = new User();
        user.setUsername("TEST_USER_1");
        Message message = new Message();
        message.setId(1);
        message.setText("Old text");
        message.setUser(user);

        Mockito.when(mockMessageRepository.findById(Matchers.any())).thenReturn(Optional.of(message));
        Mockito.doNothing().when(mockMessageRepository).delete(Matchers.any());

        Assertions.assertDoesNotThrow(() -> messageService.validateMessageAccessAndDelete(1, "TEST_USER_1"));
    }

    @Test
    public void testcreateNewMessageByUsername_NonExistantUser() {
        Mockito.when(mockUserService.getUserByUsername(Matchers.any())).thenReturn(null);

        Assertions.assertThrows(UserNotFoundException.class, ()->messageService.createNewMessageByUsername(new Message(), "TEST_USER_1"));
    }

    @Test
    public void testcreateNewMessageByUsername_ValidCase() {
        User user = new User();
        user.setUsername("TEST_USER_1");
        Message message = new Message();
        message.setId(1);
        message.setText("Text");
        message.setUser(user);

        Mockito.when(mockUserService.getUserByUsername(Matchers.any())).thenReturn(user);
        Mockito.when(mockMessageRepository.save(Matchers.any())).thenReturn(message);

        Assertions.assertDoesNotThrow(()->messageService.createNewMessageByUsername(new Message(), "TEST_USER_1"));
        Assertions.assertEquals(message, messageService.createNewMessageByUsername(new Message(), "TEST_USER_1"));
    }

    @Test
    public void testgetMessagesForAllUsers_EmptyDataStore() {
        Mockito.when(mockMessageRepository.findAll()).thenReturn(new ArrayList<>());

        List<Message> retrievedList = messageService.getMessagesForAllUsers();

        Assertions.assertNotNull(retrievedList);
        Assertions.assertTrue(retrievedList.isEmpty());
    }

    @Test
    public void testgetMessagesForAllUsers_MultipleMessages() {
        User user = new User();
        user.setUsername("TEST_USER_1");
        Message message1 = new Message();
        message1.setId(1);
        message1.setText("Text");
        message1.setUser(user);

        Message message2 = new Message();
        message2.setId(2);
        message2.setText("Text");
        message2.setUser(user);

        User secondUser = new User();
        secondUser.setUsername("TEST_USER_2");
        Message message3 = new Message();
        message3.setId(3);
        message3.setText("Text");
        message3.setUser(secondUser);


        Mockito.when(mockMessageRepository.findAll()).thenReturn(Arrays.asList(message1, message2, message3));

        List<Message> retrievedList = messageService.getMessagesForAllUsers();

        Assertions.assertNotNull(retrievedList);
        Assertions.assertFalse(retrievedList.isEmpty());
        Assertions.assertEquals(3, retrievedList.size());
    }
}