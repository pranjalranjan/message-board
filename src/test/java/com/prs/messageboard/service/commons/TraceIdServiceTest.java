package com.prs.messageboard.service.commons;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;

public class TraceIdServiceTest
{
    private TraceIdService traceIdService = new TraceIdService();

    @Test
    public void getTraceIdReturnsTheExactTraceIdPresentInMDC() {
        String traceId = "TRACE_ID_1";
        MDC.put("TRACE_ID", traceId);

        Assertions.assertEquals("TRACE_ID_1", traceIdService.getTraceId());

        traceId = "TRACE_ID_2";
        MDC.put("TRACE_ID", traceId);
        Assertions.assertEquals("TRACE_ID_2", traceIdService.getTraceId());
    }
}
