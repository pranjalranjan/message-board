package com.prs.messageboard.exception.handler;

import com.prs.messageboard.exception.message.MessageBoardAuthenticationException;
import com.prs.messageboard.exception.message.MessageNotFoundException;
import com.prs.messageboard.exception.message.UnauthorizedMessageAccessException;
import com.prs.messageboard.exception.user.UserNotFoundException;
import com.prs.messageboard.service.commons.TraceIdService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;

import java.lang.reflect.Method;

public class MessageBoardExceptionHandlerTest
{
    private TraceIdService traceIdService = new TraceIdService();
    private MessageBoardExceptionHandler messageBoardExceptionHandler = new MessageBoardExceptionHandler(traceIdService);
    private WebRequest mockWebRequest = Mockito.mock(WebRequest.class);
    private MethodArgumentNotValidException methodArgumentNotValidException = Mockito.mock(MethodArgumentNotValidException.class);

    @Test
    public void testhandleMessageBoardExceptions_UserNotFoundException() {
        ResponseEntity responseEntity = messageBoardExceptionHandler.handleMessageBoardExceptions(new UserNotFoundException("SOME_MESSAGE"), mockWebRequest);
        Assertions.assertEquals(404, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testhandleMessageBoardExceptions_MessageBoardAuthenticationException() {
        ResponseEntity responseEntity = messageBoardExceptionHandler.handleMessageBoardExceptions(new MessageBoardAuthenticationException("SOME_MESSAGE"), mockWebRequest);
        Assertions.assertEquals(401, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testhandleMessageBoardExceptions_MessageNotFoundException() {
        ResponseEntity responseEntity = messageBoardExceptionHandler.handleMessageBoardExceptions(new MessageNotFoundException("SOME_MESSAGE"), mockWebRequest);
        Assertions.assertEquals(404, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testhandleMessageBoardExceptions_UnauthorizedMessageAccessException() {
        ResponseEntity responseEntity = messageBoardExceptionHandler.handleMessageBoardExceptions(new UnauthorizedMessageAccessException("SOME_MESSAGE"), mockWebRequest);
        Assertions.assertEquals(403, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testhandleMessageBoardExceptions_AllOtherExceptions() {
        ResponseEntity responseEntity = messageBoardExceptionHandler.handleMessageBoardExceptions(new Exception("SOME_MESSAGE"), mockWebRequest);
        Assertions.assertEquals(500, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testhandleMessageBoardExceptions_MethodArgumentNotValidException() {
        ResponseEntity responseEntity = messageBoardExceptionHandler.handleMethodArgumentNotValid(methodArgumentNotValidException, HttpHeaders.EMPTY, HttpStatus.BAD_REQUEST, mockWebRequest);
        Assertions.assertEquals(400, responseEntity.getStatusCodeValue());
    }
}
