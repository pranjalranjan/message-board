CREATE TABLE `user` (
  `id` integer NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `birth_date` timestamp NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_NAME_UNIQUE` (`username`)
);

CREATE TABLE `message` (
  `id` integer NOT NULL AUTO_INCREMENT,
  `text` varchar(1024) NOT NULL,
  `posted_at` timestamp NOT NULL,
  `user_id` integer NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES user(`id`)
);