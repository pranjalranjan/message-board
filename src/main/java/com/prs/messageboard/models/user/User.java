package com.prs.messageboard.models.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.prs.messageboard.models.message.Message;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Message board user")
@Entity
@Table(name = "user")
public class User implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;

    @Size(min=1, message = "Name should have atleast 1 characters")
    @ApiModelProperty(notes = "Name should have atleast 1 character")
    private String username;

    @Past(message = "Birth date should not be a future date")
    @ApiModelProperty(notes = "Birth date should be a date in the past")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Date birthDate;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Size(min=1, message = "Password should have atleast 8 characters")
    private String password;

    @OneToMany(mappedBy = "user")
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private List<Message> messages;
}
