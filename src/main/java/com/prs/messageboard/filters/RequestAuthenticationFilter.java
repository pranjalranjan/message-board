package com.prs.messageboard.filters;

import com.prs.messageboard.service.user.JwtService;
import com.prs.messageboard.service.user.MessageBoardUserDetailsService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Adds JWT authentication to each request,
 */
@Component
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class RequestAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private MessageBoardUserDetailsService messageBoardUserDetailsService;

    @Autowired
    private JwtService jwtService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        Optional<String> username = getUsernameFromRequestByAuthorizationHeader(request.getHeader("Authorization"));

        if(username.isPresent() && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = messageBoardUserDetailsService.loadUserByUsername(username.get());

            if(jwtService.validateToken(request.getHeader("Authorization").substring(7), userDetails)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            } else {
                log.warn("Invalid jwt supplied with authorization header.");
            }
        }
        filterChain.doFilter(request, response);
    }

    private Optional<String> getUsernameFromRequestByAuthorizationHeader(String authorizationHeader) {
        String username = null;
        String jwt;

        if((authorizationHeader != null) && (authorizationHeader.startsWith("Bearer "))) {
            jwt = authorizationHeader.substring(7);
            username = jwtService.extractUsername(jwt);
        } else {
            log.warn("No authorization header containing bearer token supplied with request.");
        }

        return (username == null) ? Optional.empty() : Optional.of(username);
    }
}
