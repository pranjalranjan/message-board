package com.prs.messageboard.service.message;

import com.prs.messageboard.exception.message.MessageNotFoundException;
import com.prs.messageboard.exception.message.UnauthorizedMessageAccessException;
import com.prs.messageboard.exception.user.UserNotFoundException;
import com.prs.messageboard.models.message.Message;
import com.prs.messageboard.models.user.User;
import com.prs.messageboard.repositories.message.MessageRepository;
import com.prs.messageboard.service.user.UserService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Service to manage the message resource
 */
@Service
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class MessageService
{
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserService userService;

    /**
     * Retrieves message by its message id, if it belongs to the authenticated user.
     * @param messageId The id of the message to be retrieved.
     * @param username The authenticated user's username.
     * @return The message mapped to the message id.
     */
    @Transactional(readOnly = true)
    public Message getMessageByMessageIdAndUserName(Integer messageId, String username) {
        Optional<Message> messageOptional = messageRepository.findById(messageId);

        if(! messageOptional.isPresent()) {
            log.error("Message with id: " + messageId + " does not exist in data store.");
            throw new MessageNotFoundException("Message with message id: " + messageId + " not found.");
        }

        Message message = messageOptional.get();

        if(! message.getUser().getUsername().equals(username)) {
            log.error("Unauthorised access attempt by user: " + username + " for message: " + messageId);
            throw new UnauthorizedMessageAccessException("Message : " + messageId + " is not accessible by user: " + username);
        }
        return message;
    }

    /**
     * Updates a message represented by it message id, if it belongs to the authenticated user.
     * @param newMessage  The new message object.
     * @param messageId The id of the message to be updated.
     * @param username The authenticated user's username.
     * @return The updated message object.
     */
    @Transactional
    public Message validateMessageAccessAndUpdateText(Message newMessage, Integer messageId, String username) {
        Message originalMessage = getMessageByMessageIdAndUserName(messageId, username);
        originalMessage.setPostedAt(new Date());
        originalMessage.setText(newMessage.getText());
        Message savedMessage = messageRepository.save(originalMessage);
        log.info("Message : " + messageId + "successfully updated with new text.");
        return savedMessage;
    }

    /**
     * Deletes a message represented by it message id, if it belongs to the authenticated user.
     * @param messageId The id of the message to be deleted.
     * @param username The authenticated user's username.
     */
    @Transactional
    public void validateMessageAccessAndDelete(Integer messageId, String username) {
        messageRepository.delete(getMessageByMessageIdAndUserName(messageId, username));
        log.info("Message : " + messageId + "successfully deleted");
    }

    /**
     * Creates a new message for a user.
     * @param messageTemplate Message to be created.
     * @param username The authenticated user's username.
     * @return The created message object
     */
    @Transactional
    public Message createNewMessageByUsername(Message messageTemplate, String username) {
        User user = userService.getUserByUsername(username);

        if (user == null) {
            throw new UserNotFoundException("User with username: " + username + " not found");
        }

        messageTemplate.setPostedAt(new Date());
        messageTemplate.setUser(user);
        Message savedMesage = messageRepository.save(messageTemplate);
        log.info("New message : " + savedMesage.getId() + " created for user: " + username);
        return savedMesage;
    }

    /**
     * Fetches all messages.
     * @return List of messages belonging to all users.
     */
    @Transactional(readOnly = true)
    public List<Message> getMessagesForAllUsers() {
        return messageRepository.findAll();
    }

    /**
     * Fetches messages for all users honouring a size limit and sorted by timestamp.
     * @param sizeLimit The maximum number of messages to be sent.
     * @return List of messages belonging to all users, size capped by size limit.
     */
    @Transactional(readOnly = true)
    public List<Message> getMessagesForAllUsersWithSizeLimitSortedByTimestamp(int sizeLimit) {
        return getPaginatedResult(0, sizeLimit).toList();
    }

    /**
     * Fetches messages for all users honouring a size limit, a page offset and sorted by timestamp.
     * @param sizeLimit The maximum number of messages to be sent per page.
     * @param offset The current page number to be considered during retrieval
     * @return List of messages belonging to all users, size capped by size limit and result belonging to the relevant page.
     */
    @Transactional(readOnly = true)
    public List<Message> getMessagesForAllUsersPaginatedWithSizeLimitSortedByTimestamp(int sizeLimit, int offset) {
        return getPaginatedResult(offset, sizeLimit).toList();
    }

    private Page<Message> getPaginatedResult(int offset, int sizeLimit) {
        return messageRepository.findAll(PageRequest.of(offset, sizeLimit, Sort.by(Sort.Direction.DESC, "postedAt")));
    }
}
