package com.prs.messageboard.service.user;

import com.prs.messageboard.exception.user.UserNotFoundException;
import com.prs.messageboard.models.message.Message;
import com.prs.messageboard.models.user.User;
import com.prs.messageboard.repositories.user.UserRepository;
import com.prs.messageboard.service.message.MessageService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Component
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserService
{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageService messageService;

    /**
     * Fetch a user by its username.
     * @param username The username of the user to be fetched.
     * @return The user object mapped to the username
     */
    @Transactional(readOnly = true)
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    /**
     * Get all the users in the system.
     * @return List of all the users in the system.
     */
    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Fetch a user by its id.
     * @param userId The user id of the user to be fetched.
     * @return The user object mapped to the user id
     */
    @Transactional(readOnly = true)
    public User getUserById(Integer userId) {
        Optional<User> user = userRepository.findById(userId);

        if(!user.isPresent()) {
            log.error("User with id : " + userId +" does not exist in data store.");
            throw new UserNotFoundException("User with id : " + userId +" was not found.");
        }

        return user.get();
    }

    /**
     * Create a new user in the system.
     * @param newUser Credentials of the new user.
     * @return Details of the created user.
     */
    @Transactional
    public User createNewUser(User newUser) {
        return userRepository.save(newUser);
    }

    /**
     * Delete will perform a complete clean up of the user from the system
     * which also includes all the messages they have.
     * @param userId
     */
    @Transactional
    public void deleteUserById(Integer userId) {

        User toBeDeletedUser = getUserById(userId);

        for (Message message: toBeDeletedUser.getMessages()) {
            messageService.validateMessageAccessAndDelete(message.getId(), toBeDeletedUser.getUsername());
        }
        userRepository.delete(toBeDeletedUser);
    }
}
