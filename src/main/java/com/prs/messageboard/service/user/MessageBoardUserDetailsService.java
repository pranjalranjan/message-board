package com.prs.messageboard.service.user;

import com.prs.messageboard.exception.user.UserNotFoundException;
import com.prs.messageboard.models.user.User;
import com.prs.messageboard.repositories.user.UserRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Customized user details service utilising all users present in the user data store.
 */
@Service
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class MessageBoardUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    /**
     * Fetches user details from the user data store and loads it in the security context.
     * @param username The username of the user to be loaded in the context.
     * @return UserDetails object containing attributes of the user to be authenticated and stored in Security Context.
     * @throws UsernameNotFoundException if the user with username was not found in the data store.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User messageBoardUser = userRepository.findByUsername(username);

        if(messageBoardUser == null){
            log.error("User with username : " + username + " does not exist in data store.");
            throw new UserNotFoundException("User with username : " + username + " not found");
        }
        return new org.springframework.security.core.userdetails.User(messageBoardUser.getUsername(), messageBoardUser.getPassword(), new ArrayList<>());
    }
}
