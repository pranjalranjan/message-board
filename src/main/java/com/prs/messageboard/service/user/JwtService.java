package com.prs.messageboard.service.user;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Service to handle Jwt related concerns.
 */
@Service
@Slf4j
public class JwtService
{
    private final String SECRET_KEY = "HARDCODED_SECRET_KEY";

    /**
     * Generates a new JWT for a given user.
     * @param userDetails UserDetails object containing user attributes.
     * @return The JWT to be subsequently used by client.
     */
    public String generateToken(UserDetails userDetails) {
        return createToken(new HashMap<String, Object>(), userDetails.getUsername());
    }

    private String createToken(Map<String, Object> claims, String subject) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000*60*60*10))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    /**
     * Verifies if a token is expired or not.
     * @param token The JWT under consideration.
     * @return Boolean parameter to specify if token is expired or not.
     */
    private Boolean isTokenExpired(String token) {
        if (extractExpiration(token).before(new Date())) {
            log.warn("Jwt supplied with authorization header is expired.");
            return true;
        }
        return false;
    }

    /**
     * Validates a token by checking expiration and matching username.
     * @param token The JWT under consideration.
     * @param userDetails The UserDetails object under consideration containing all attributes for the user.
     * @return Boolean parameter to specify if token is valid or not.
     */
    public Boolean validateToken(String token, UserDetails userDetails) {
        return ((! (isTokenExpired(token))) && (extractUsername(token).equals(userDetails.getUsername())));
    }

    /**
     * Extracts all claims out from the JWT using the secret key.
     * @param token The JWT under consideration.
     * @return All the claims present in the Jwt.
     */
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    /**
     * Extract user's username from the JWT claims.
     * @param token The JWT under consideration.
     * @return username of the user.
     */
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    /**
     * Extract token expiry from the JWT claims.
     * @param token The JWT under consideration.
     * @return token expiration time.
     */
    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        return claimsResolver.apply(extractAllClaims(token));
    }

}
