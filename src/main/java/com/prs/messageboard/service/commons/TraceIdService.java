package com.prs.messageboard.service.commons;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;

@Component
public class TraceIdService
{
    public String getTraceId() {
        return MDC.get("TRACE_ID");
    }
}
