package com.prs.messageboard.configuration;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * Interceptor to intercept each request and add trace Id to Mapped Diagnostic Context(MDC)
 */
@Component
public class TraceIdInterceptor implements HandlerInterceptor {
    private String TRACE_ID_HEADER = "X-Trace-Id";

    /**
     * Adds a randomly generate UUID which will act as request trace Id subsequently to the MDC.
     */
    public TraceIdInterceptor() {
        MDC.put("TRACE_ID", UUID.randomUUID().toString());
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String traceId = UUID.randomUUID().toString();
        String requestTraceIdHeaderValue = request.getHeader(TRACE_ID_HEADER);
        if((requestTraceIdHeaderValue != null) && (! requestTraceIdHeaderValue.trim().isEmpty())) {
            traceId = requestTraceIdHeaderValue;
        }
        MDC.put("TRACE_ID", traceId);
        return true;
    }
}
