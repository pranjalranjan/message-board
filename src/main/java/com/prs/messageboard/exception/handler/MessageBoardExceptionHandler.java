package com.prs.messageboard.exception.handler;

import com.prs.messageboard.exception.message.MessageBoardAuthenticationException;
import com.prs.messageboard.exception.message.MessageNotFoundException;
import com.prs.messageboard.exception.message.UnauthorizedMessageAccessException;
import com.prs.messageboard.exception.user.UserNotFoundException;
import com.prs.messageboard.models.http.ErrorResponse;
import com.prs.messageboard.service.commons.TraceIdService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@RestController
@ControllerAdvice
@AllArgsConstructor
@NoArgsConstructor
public class MessageBoardExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private TraceIdService traceIdService;

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleMessageBoardExceptions(Exception exception, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(new Date(), exception.getMessage(), traceIdService.getTraceId());
        return new ResponseEntity(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<Object> handleMessageBoardExceptions(UserNotFoundException userNotFoundException, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(new Date(), userNotFoundException.getMessage(), traceIdService.getTraceId());
        return new ResponseEntity(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MessageBoardAuthenticationException.class)
    public final ResponseEntity<Object> handleMessageBoardExceptions(MessageBoardAuthenticationException messageBoardAuthenticationException, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(new Date(), messageBoardAuthenticationException.getMessage(), traceIdService.getTraceId());
        return new ResponseEntity(errorResponse, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(MessageNotFoundException.class)
    public final ResponseEntity<Object> handleMessageBoardExceptions(MessageNotFoundException messageNotFoundException, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(new Date(), messageNotFoundException.getMessage(), traceIdService.getTraceId());
        return new ResponseEntity(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UnauthorizedMessageAccessException.class)
    public final ResponseEntity<Object> handleMessageBoardExceptions(UnauthorizedMessageAccessException unauthorizedMessageAccessException, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(new Date(), unauthorizedMessageAccessException.getMessage(), traceIdService.getTraceId());
        return new ResponseEntity(errorResponse, HttpStatus.FORBIDDEN);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(new Date(), "Input validation failed", traceIdService.getTraceId());
        return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
}
