package com.prs.messageboard.exception.message;

public class MessageBoardAuthenticationException extends RuntimeException
{
    public MessageBoardAuthenticationException(String message) {
        super(message);
    }
}
