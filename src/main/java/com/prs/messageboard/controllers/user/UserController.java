package com.prs.messageboard.controllers.user;

import com.prs.messageboard.models.user.User;
import com.prs.messageboard.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

import javax.validation.*;

/**
 * Handles the user resource
 */
@RestController
@Slf4j
public class UserController
{
    @Autowired
    private UserService userService;

    /**
     * Get all users in the system
     * @return List of all the users in the system.
     */
    @GetMapping(path = "/user")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    /**
     * Fetches a user given its user id.
     * @param userId The id of the user to be retrieved.
     * @return The user mapped to the id.
     */
    @GetMapping("/user/{userId}")
    public User getUserById(@PathVariable Integer userId) {
        return userService.getUserById(userId);
    }

    /**
     * Create a new user
     * @param user The user object to be created, and its attributes.
     * @return The created user object's URI.
     */
    @PostMapping("/user")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
        User savedUser = userService.createNewUser(user);
        log.info("New user created : " + user.getUsername());
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    /**
     * Delete a user.
     * @param userId The id of the user to be deleted.
     */
    @DeleteMapping("/user/{userId}")
    public void deleteUser(@PathVariable Integer userId) {
        userService.deleteUserById(userId);
        log.info("User : " + userId + " deleted.");
    }
}
