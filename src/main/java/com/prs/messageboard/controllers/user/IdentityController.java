package com.prs.messageboard.controllers.user;

import com.prs.messageboard.exception.message.MessageBoardAuthenticationException;
import com.prs.messageboard.models.http.request.AuthenticationRequest;
import com.prs.messageboard.models.http.response.AuthenticationResponse;
import com.prs.messageboard.service.commons.TraceIdService;
import com.prs.messageboard.service.user.JwtService;
import com.prs.messageboard.service.user.MessageBoardUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Handles the identity resource to manage authentication.
 */
@RestController
@Slf4j
public class IdentityController
{
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MessageBoardUserDetailsService messageBoardUserDetailsService;

    @Autowired
    private JwtService jwtService;

    /**
     * Authenticates the user by matching the password stored in the data store
     * to the user-provided password.
     * @param authenticationRequest The user provided credentials.
     * @return JWT belonging to the user.
     */
    @PostMapping(path = "/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticateUser(@RequestBody AuthenticationRequest authenticationRequest) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));
        } catch (BadCredentialsException ex) {
            log.error("Authentication failure: Incorrect credentials supplied by user : " + authenticationRequest.getUsername() + " at " + new Date());
            throw new MessageBoardAuthenticationException("Username or password is incorrect");
        }

        UserDetails userDetails = messageBoardUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        return ResponseEntity.ok(new AuthenticationResponse(jwtService.generateToken(userDetails)));
    }
}
