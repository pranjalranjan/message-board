package com.prs.messageboard.controllers.message;

import com.prs.messageboard.models.message.Message;
import com.prs.messageboard.service.message.MessageService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.net.URI;
import java.util.List;
import java.util.Optional;

/**
 * Handles the message resource.
 */
@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    /**
     * The default size limit to be used honoured in get message operations.
     */
    @Value("${messageboard.get.size_limit.default}")
    private Integer defaultGetOperationSizeLimit;

    /**
     * Create a new message for user derived from authorization header.
     * @param message The message text to be created.
     * @param authentication The authentication context.
     * @return Created if message is successfully created.
     */
    @PostMapping(path = "/message", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createMessage(@RequestBody Message message, Authentication authentication) {
        Message savedMessage = messageService.createNewMessageByUsername(message, authentication.getName());
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{messageId}").buildAndExpand(savedMessage.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    /**
     * Update an existing message belonging to the user derived from the authorization header.
     * @param newMessage The updated text of the message.
     * @param messageId The Id of the message to be updated.
     * @param authentication The authentication context.
     * @return Ok if message is successfully updated.
     */
    @PutMapping(path = "/message/{messageId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateMessage(@RequestBody Message newMessage, @PathVariable Integer messageId, Authentication authentication) {
        Message updatedMessage = messageService.validateMessageAccessAndUpdateText(newMessage, messageId, authentication.getName());
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{messageId}").buildAndExpand(updatedMessage.getId()).toUri();
        return ResponseEntity.ok().location(location).build();
    }

    /**
     * Delete an existing message belonging to the user derived from the authorization header.
     * @param messageId The Id of the message to be deleted.
     * @param authentication The authentication context.
     */
    @DeleteMapping(path = "/message/{messageId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteMessage(@PathVariable Integer messageId, Authentication authentication) {
        messageService.validateMessageAccessAndDelete(messageId, authentication.getName());
    }

    /**
     * Get all messages in the system
     * @param limit The maximum number of messages to be returned. If not provided, the default size
     *              limit is honoured. If provided, the effective size limit is minimum of the default
     *              size limit value and the provided value.
     * @param offset The page to be read in a paginated query. If not provided, 1st page is assumed by
     *               default.
     * @return List of all the messages in the system
     */
    @GetMapping(path = "/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Message> getAllMessages(
            @ApiParam(name = "limit", type = "Integer", value = "Size limit of the messages required", example = "1", defaultValue = "5")
            @RequestParam(required = false) @Min(value = 1, message = "Size limit should be atleast 1 if defined.") Optional<Integer> limit ,
            @ApiParam(name = "offset", type = "Integer", value = "Page number of required response in paginated query", example = "2")
            @RequestParam(required = false) @Min(0) Optional<Integer> offset) {

        int effectiveSizeLimit = Math.min(defaultGetOperationSizeLimit, limit.orElse(defaultGetOperationSizeLimit));

        if(offset.isPresent())
            return messageService.getMessagesForAllUsersPaginatedWithSizeLimitSortedByTimestamp(effectiveSizeLimit, offset.get());

        return messageService.getMessagesForAllUsersWithSizeLimitSortedByTimestamp(effectiveSizeLimit);
    }
}
