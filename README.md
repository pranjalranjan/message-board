# README #

### What is this repository for? ###

* Quick summary: This is a public message board where users can post, edit and delete their messages and view messages posted by others.
* Version 1.0.0

### How do I get set up? ###

### Summary of set up ###

  Message board is a spring boot application (hence, uses Java) which allows users to post messages, edit and delete their own messages and view all messages posted by others.
  The endpoints exposed rely on user authentication to fetch a token, which serves as proof of identity for user's subsequent requests.


  The application starts with three users with usernames: USER_1, USER_2 and USER_3 and passwords: "PASSWORD", "PASSWORD", "PASSWORD" respectively.
  **Before carrying out any operation it is expected that an authentication token be collected from the exposed /authenticate endpoint.** This token
  needs to be passed along in the Authorization header as a bearer token, with subsequent requests to allow for identification of user for carrying out these operations.
  The application does not provide any way for these details to be passed along with requests contexts like payload, query params, etc, and only reiles on the JWT in the
  authorization header to fetch the user context. New users can be registered on the service as well.


  The application provides a "/message" endpoint with relevant HTTP verbs to carry out CRUD operations related to messages. It takes care of validating user-provided parameters
  and returns appropriate response codes as per the use case.


  Swagger docs are exposed at: http://localhost:8080/v2/api-docs . Project also has a swagger UI exposed at: http://localhost:8080/swagger-ui/index.html#/


  **A valid and tested Postman API collection is also present in the project root directory.**


  Note: When a user is deleted from the "/user" endpoint, the application also deletes all the messages posted by the user across the system as part of a complete clean up operation
  , instead of creating tombstones. The assumption with the delete endpoint is that the user would want all their data to be wiped off the system.


  Note: The application honours necessary size limit and offset parameters while fetching all the messages from the system. These can be provided by the user as request parameters as well.
### Configuration ###
  The service is a spring boot application and uses the application.properties file to run. It is best deployed in a java-8 environment.
### Dependencies ###
  Spring boot dependencies are managed by gradle and defined in the build.gradle file.
### Database configuration ###
  The service uses an in-memory database: H2 to store data for user and message entities. The database console can be viewed at /h2-console , username: sa , no password
### How to run tests ###
  The project has a test suite comprising of integration and unit tests. The tests can be run using: 
  ```
  ./gradlew clean test -i
  ```
  A test coverage report can be generated using the following command (jaCoCo plugin is used):
  ```
  ./gradlew clean build jacocoTestReport
  ```
### Deployment instructions ###

  This message board project uses gradle as the build dependency manager. 
  
Gradle wrapper is included with the repo to allow for standalone working of the project wihout the need to install anything. 

The service utilizes http to run on "http://localhost:8080" and the following modes of run are supported:

#### Run locally on physical host ####
The following command can be used to build and run the message board application as a jar:
    ```
     ./gradlew bootRun
    ```
  OR
  ```
  ./gradlew bootJar
  java -jar build/libs/messageboard-0.0.1-SNAPSHOT.jar
  ```


#### Run in a docker container ####
       
The project is published as a public image on docker hub as well: pranjalranjan/message-board:1.0.0 and a working container can be
       spawned using:
       ```
       docker run -p 8080:8080 pranjalranjan/message-board:1.0.0
       ```
  OR


  To Build the image locally and then run the container:
  ```
  docker build -t pranjalranjan/message-board:1.0.0 .
  docker run -p 8080:8080 pranjalranjan/message-board:1.0.0
  ```


#### Run in a Kubernetes cluster ####
       
Use the messageboard_deployment.yaml and messageboard_service.yaml in the project root directory to deploy on the Kubernetes cluster.
       By default this creates a cluster ip service and is accessible using curl. It can be exposed using a NodePort service or a Kubernetes
       ingress controller behind a load balancer.

### Some future enhancements ###

1. Add UI to support the backend.
2. Add a logical and contextual layer of topics -> Whenever a message is posted, it is linked to a topic.
3. Users can reply to messages from other users and tag each other. Enhancements on the contextual layer.
4. Store old data when user edits a message in a linked list fashion.
5. Time bound the edit message capability to only a limited time period after creation of the message.
6. Tag messages using topics and allow users to filter messages using these tags.