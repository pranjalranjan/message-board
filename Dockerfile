FROM openjdk:8-jdk-alpine
RUN addgroup -S messageboard_group && adduser -S messageboard_user -G messageboard_group
USER messageboard_user:messageboard_group
ARG JAR_FILE=build/libs/messageboard-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]